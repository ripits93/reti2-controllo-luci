#params[0]	->	BB server (port)
11020

#params[1]	->	WebSocket server uri (ws://address:port)
ws://localhost:11024/ws-server/python
#ws://localhost:8080/ws-server/java
#ws://localhost:14991

#params[2]	->	Mosquitto server (tcp://address:port)
tcp://192.168.1.13:1883

#params[3-6]	->	Tunnel ssh (params[3]=rPort, params[4]=rIp, params[5]=rUser, params[6]=rPswd)
8090
192.168.1.12
trank
ripits93

#params[7]	->	Osso server (address:port)
localhost:14990