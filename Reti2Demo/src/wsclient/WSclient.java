package wsclient;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import agent.ConnectionParams;
import agent.SubjectTable;

public class WSclient {

	private static String wsUri;

	public static void main(SubjectTable sm) {
		// TODO Auto-generated method stub
		wsUri = ConnectionParams.params[1];
		WebSocketClient client = new WebSocketClient();
		WSUtil socket = new WSUtil(sm);
		try {
			client.start();
			URI echoUri = new URI(wsUri);
			ClientUpgradeRequest request = new ClientUpgradeRequest();
			client.connect(socket, echoUri, request);
			System.out.printf("WS-CLIENT Connecting to : %s%n", echoUri);

			// wait for closed socket connection.
			socket.awaitClose(Integer.MAX_VALUE, TimeUnit.DAYS);
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			try {
				client.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


}
