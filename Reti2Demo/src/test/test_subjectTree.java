package test;


import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import agent.*;

public class test_subjectTree {
	
	Subject root;
	SubjectTable table;
	
	@Test
	public void testCreazioneTabella() {	
		root = new Subject("root");
		table = new SubjectTable(root);
		
		assert(root.getName().equals("root"));
	}
	
	@Test
	public void testAggiuntaNodi() {
		testCreazioneTabella();
		
		table.subscribe("root/figlioA", new MessageQueue("nodoA"));
		assert(root.getSons().get(0).getName().equals("figlioA"));
		
		table.subscribe("root/figlioB", new MessageQueue("nodoB"));
		assert(root.getSons().get(1).getName().equals("figlioB"));
		
		table.subscribe("root/figlioC", new MessageQueue("nodoC"));
		assert(root.getSons().get(2).getName().equals("figlioC"));
	}
	
	@Test
	public void testAggiuntaNodiLivello2A() {
		testAggiuntaNodi();

		table.subscribe("root/figlioA/figlioA1", new MessageQueue("nodoA1"));
		assert(root.getSons().get(0).getSons().get(0).getName().equals("figlioA1"));
		
		table.subscribe("root/figlioA/figlioA2", new MessageQueue("nodoA2"));
		assert(root.getSons().get(0).getSons().get(1).getName().equals("figlioA2"));
		
		table.subscribe("root/figlioA/figlioA3", new MessageQueue("nodoA3"));
		assert(root.getSons().get(0).getSons().get(2).getName().equals("figlioA3"));
	}
	
	@Test
	public void testAggiuntaNodiLivello2B() {
		testAggiuntaNodiLivello2A();
		
		table.subscribe("root/figlioB/figlioB1", new MessageQueue("nodoA1"));
		assert(root.getSons().get(1).getSons().get(0).getName().equals("figlioB1"));
		
		table.subscribe("root/figlioB/figlioB2", new MessageQueue("nodoB2"));
		assert(root.getSons().get(1).getSons().get(1).getName().equals("figlioB2"));

	}
	
	@Test
	public void testAggiuntaNodiLivello2C() {
		testAggiuntaNodiLivello2B();
		table.subscribe("root/figlioC/figlioC1", new MessageQueue("nodoC1"));
		table.subscribe("root/figlioC/figlioC1", new MessageQueue("nodoC2"));
		table.notify(new Message("root/figlioC/figlioC1", "CIAO"));
		assert(root.getSons().get(2).getSons().get(0).getName().equals("figlioC1"));
		assert(root.getSons().get(2).getSons().get(0).getMessQueue().get(0).getId().equals("nodoC1"));
		assert(root.getSons().get(2).getSons().get(0).getMessQueue().get(1).getId().equals("nodoC2"));
		assert(root.getSons().get(2).getSons().get(0).getMessQueue().get(0).getMessages().get(0).getBody().equals("CIAO"));
		assert(root.getSons().get(2).getSons().get(0).getMessQueue().get(1).getMessages().get(0).getBody().equals("CIAO"));
		
	}
	

}
