package mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import agent.ConnectionParams;
import agent.Message;
import agent.MessageQueue;
import agent.SubjectTable;

public class MosquittoClient extends Thread{
	
	
	private MqttClient client, send;
	private SubjectTable st;
	private MessageQueue mq;
	
	public MosquittoClient(String url, SubjectTable st) throws MqttException {
		this.st = st;

		client = new MqttClient(url, "BB");
		client.setCallback(new SimpleMqttCallBack(this, st));
		client.connect();
		client.subscribe("HORUS");
		
		send = new MqttClient(url, "BB"+1);
		
		this.start();
	
	}
	public void run() {

		mq = new MessageQueue("mqttClient");
		st.subscribe("root/threads/dynamicPage", mq);
		
		while(true){
			Message m = mq.receive();
			if (m.getMethod().equals("PUT")){
				String response = m.getBody();
				System.out.println("MQTT TO SEND: " + response);	
				try {
					publishToMqtt(response.replace("\"time\"", "\"op\":\"PUT\",\"time\""));
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		
	}
	public void publishToMqtt(String m) throws MqttPersistenceException, MqttException {
		send.connect();
		MqttMessage message = new MqttMessage();
		message.setPayload(m.getBytes());
		send.publish("CLIENT", message);
		send.disconnect();
	}

}
