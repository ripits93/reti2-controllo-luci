package mqtt;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import agent.ConnectionParams;
import agent.Message;
import agent.MessageQueue;
import agent.SubjectTable;

public class SimpleMqttCallBack implements MqttCallback {

	SubjectTable st;
	MessageQueue mqGet, mqPut;
	MosquittoClient mqc;

	public SimpleMqttCallBack(MosquittoClient mqc, SubjectTable st) {
		this.st = st;
		this.mqc = mqc;
	}

	public void connectionLost(Throwable throwable) {
		System.out.println("Connection to MQTT broker lost!");
		throwable.printStackTrace();
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
		String received = new String(arg1.getPayload());
		System.out.println("Message received:\n\t" + received);
		String[] messageParams = received.split("#");
		System.out.println(messageParams[0]);
		if (messageParams[0].equals("PUT")) {
			Process p = Runtime.getRuntime()
					.exec("curl localhost:" + ConnectionParams.params[0] + "" + messageParams[1] + " -X PUT");
		} else {
			Process p = Runtime.getRuntime().exec("curl localhost:" + ConnectionParams.params[0] + "/dynamicPage");
			String response = "";
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;

			while ((line = in.readLine()) != null) {
				response += line;
			}

			mqc.publishToMqtt(response.replace("{\"luci\"", "{\"op\":\"GET\",\"luci\""));
		}

	}

}