package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import agent.ConnectionParams;
import agent.SubjectTable;

public class HorusHandler implements HttpHandler {

	static String reqUri = new String("");
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		reqUri = he.getRequestURI().toString();
		System.out.println("HORUS HANDLE reqUri: " + reqUri);
		System.out.println("HORUS HANDLE ipClient: " + he.getRemoteAddress().toString());
		
		Process p = Runtime.getRuntime().exec("curl "+ConnectionParams.params[7]+"" + reqUri );
		
		BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		
		String	response = new String("");
		while ((line = in.readLine()) != null) {
		    response += line;
		}
		
		System.out.println("HORUS HANDLE response: " + response);
		
		he.sendResponseHeaders(200,	response.length());
		
		OutputStream	os	=	(OutputStream) he.getResponseBody();
		os.write(response.getBytes());
		os.close();

	}

}
