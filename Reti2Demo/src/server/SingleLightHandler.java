package server;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import agent.Agente;
import agent.Message;
import agent.MessageQueue;
import agent.SubjectTable;

public class SingleLightHandler implements HttpHandler {

	String[] luci = StartServer.luci;
	int idLuce;
	String path = "";
	SubjectTable st;
	MessageQueue mq;
	
	public SingleLightHandler(SubjectTable st, int luce, boolean on){
		this.st = st;
		if(on) {
			this.mq = new MessageQueue("luceOnHandler");
			this.st.subscribe("root/handlers/accendiHandler/" + luci[luce], mq);
			this.path = "accendi";
		}
		else {
			this.mq = new MessageQueue("LuceOffHandler");
			this.st.subscribe("root/handlers/spegniHandler/" + luci[luce], mq);
			this.path = "spegni";
		}
		this.idLuce = luce;
	}
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		System.out.println("LUCE" + (idLuce + 1) + " HANDLE Ip client: " + he.getRemoteAddress().toString());

		String response = "";
		
		if (he.getRequestMethod().equals("PUT")) {

			Message msg = new Message("root/threads/" + path, String.valueOf(idLuce));
			this.st.notify(msg);

			// wait for thread response...
			Message thrRe = this.mq.receive();
			response = thrRe.getBody();

			System.out.println("LUCE" + (idLuce + 1) + " HANDLE response: " + response);
		} else {
			response = "<h1>Risorsa non trovata!</h1> \n";
		}

		he.sendResponseHeaders(200, response.length());

		OutputStream os = (OutputStream) he.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}

}
