package server;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

import agent.ConnectionParams;
import agent.SubjectTable;

public class StartServer {

	static String[] luci = new String[] { "LI_Luce1", "LI_Luce2", "LI_Luce3",
			"LI_Luce4", "LI_Luce5", "LI_Luce6","LI_Luce7", "LI_Luce8"};
	
	public static void main(SubjectTable st) throws IOException {	
		int	port = Integer.parseInt(ConnectionParams.params[0]);
		HttpServer	server	=	HttpServer.create(new InetSocketAddress(port),	0);
		System.out.println("server	started	at	"	+	port);
		
		server.createContext("/", new RootHandler(st));
		server.createContext("/doc/", new DocHandler());
		server.createContext("/dynamicPage", new GetLuciHandler(st));
		server.createContext("/horus-ws/Devices/", new HorusHandler());
		server.createContext("/horus-ws/Devices/all/On", new AllLightHandler(st,true));
		server.createContext("/horus-ws/Devices/all/Off", new AllLightHandler(st, false));
		
		for(int i=0; i<8; i++){
			server.createContext("/horus-ws/Devices/" + luci[i] + "/ON.json", new SingleLightHandler(st, i, true));
		}
		
		for(int i=0; i<8; i++){
			server.createContext("/horus-ws/Devices/" + luci[i] + "/OFF.json", new SingleLightHandler(st, i, false));
		}

		server.setExecutor(null);
		server.start();
	}

}