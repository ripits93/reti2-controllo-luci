package server;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import agent.Message;
import agent.MessageQueue;
import agent.SubjectTable;


public class GetLuciHandler implements HttpHandler {
	
	SubjectTable st;
	MessageQueue mq;
	
	public GetLuciHandler(SubjectTable st) {
		this.st = st;
		this.mq = new MessageQueue("getLuciHandler");
		this.st.subscribe("root/handlers/getLuci", mq);
	}
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		System.out.println("GETLUCI HANDLE Ip client: " + he.getRemoteAddress().toString());
		
		Message m = new Message("GET", "root/threads/dynamicPage", "");
		st.notify(m);
		
		String body = mq.receive().getBody();
		String response = body;
	
		he.sendResponseHeaders(200,	response.length());
		
		OutputStream	os	=	(OutputStream) he.getResponseBody();
		os.write(response.getBytes());
		os.close();
		
	}

}
