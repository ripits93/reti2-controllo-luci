package server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import agent.Agente;
import agent.Message;
import agent.MessageQueue;
import agent.SubjectTable;

public class AllLightHandler implements HttpHandler {
	
	String[] luci = StartServer.luci;	
	SubjectTable st;
	MessageQueue mq;
	String path = "";
	
	public AllLightHandler(SubjectTable st, boolean on) {
		this.st = st;
		if(on) {
			this.mq = new MessageQueue("allLuciOnHandler");
			this.st.subscribe("root/handlers/accendiHandler/all", mq);
			this.path = "accendi";
		}
		else {
			this.mq = new MessageQueue("allLuciOffHandler");
			this.st.subscribe("root/handlers/spegniHandler/all", mq);
			this.path = "spegni";
		}
	}
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		System.out.println("AllLuciOn HANDLE");
		System.out.println("Ip client: " + he.getRemoteAddress().toString());
		
		String response = "";
		
		if(he.getRequestMethod().equals("PUT")){
			response = "<h1>Accendo tutte le luci!</h1>";
			
			Message msg = new Message("root/threads/" + path, "ALL");
			this.st.notify(msg);
			
			response = this.mq.receive().getBody();
		} else {
			response = "<h1>Risorsa non trovata!</h1>";
		}
	
		he.sendResponseHeaders(200,	response.length());
		
		OutputStream	os	=	(OutputStream) he.getResponseBody();
		os.write(response.getBytes());
		os.close();
		
	}

}