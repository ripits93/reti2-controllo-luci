package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import agent.ConnectionParams;
import agent.SubjectTable;
import mqtt.MosquittoClient;

public class RootHandler implements HttpHandler {

	static String reqUri = new String("");
	private MosquittoClient mqttClient = null;
	private boolean tunnel = false;
	private SubjectTable st;
	
	public RootHandler(SubjectTable st){
		this.st = st;
	}
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		reqUri = he.getRequestURI().toString();
		System.out.println("ROOT HANDLE reqUri: " + reqUri);
		System.out.println("ROOT HANDLE Ip client: "+ he.getRemoteAddress().toString());
		
		if(reqUri.equals("/mqtt")){
			String res = "";
			if(mqttClient == null){
				try {
					mqttClient = connectToMqtt();
					res = "Connected to mqtt server";
				} catch (MqttException e) {
					 res = e.getMessage();
					 e.printStackTrace();
				}
				finally{
					he.sendResponseHeaders(200, res.length());
					OutputStream os = (OutputStream) he.getResponseBody();
					os.write(res.toString().getBytes());
					os.close();
				}
			}
			else{
				res = "Already connected to mqtt server";
				he.sendResponseHeaders(200, res.length());
				OutputStream os = (OutputStream) he.getResponseBody();
				os.write(res.toString().getBytes());
				os.close();
			}
			return;
		}
		
		
		if(reqUri.equals("/tunnel")){
			String res = "";
			
			if(!tunnel){
				try {
					sshTunnel(Integer.parseInt(ConnectionParams.params[0]), Integer.parseInt(ConnectionParams.params[3]));
					res = "Tunnel created";
					tunnel = true;
				} catch (Exception e) {
					 res = e.getMessage();
					 e.printStackTrace();
				}
				finally{
					he.sendResponseHeaders(200, res.length());
					OutputStream os = (OutputStream) he.getResponseBody();
					os.write(res.toString().getBytes());
					os.close();
				}
			}
			else{
				res = "Tunnel already created";
				he.sendResponseHeaders(200, res.length());
				OutputStream os = (OutputStream) he.getResponseBody();
				os.write(res.toString().getBytes());
				os.close();
			}
			return;
		}
		
		try {
			sendTextFile(he);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public static void sendTextFile(HttpExchange hex) throws Exception{
		StringBuilder text = new StringBuilder("");
		try {
			File f = new File("./doc/home.html");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				text.append(line + "\n");
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		hex.sendResponseHeaders(200, text.length());

		OutputStream os = (OutputStream) hex.getResponseBody();
		os.write(text.toString().getBytes());
		os.close();
	}
	
	public MosquittoClient connectToMqtt() throws MqttException{
		 return new MosquittoClient(ConnectionParams.params[2], st);
	}

	public void sshTunnel(int lPort, int rPort) throws Exception {
		String host = ConnectionParams.params[4];
		String user = ConnectionParams.params[5];
		String password = ConnectionParams.params[6];
		int port = 22;

		int tunnelLocalPort = lPort;
		String tunnelLocalHost =  "127.0.0.1";
		int tunnelRemotePort = rPort;

		JSch jsch = new JSch();
		Session session = jsch.getSession(user, host, port);
		session.setPassword(password);
		localUserInfo lui=new localUserInfo();
		          session.setUserInfo(lui);
		session.connect();
		session.setPortForwardingR(tunnelRemotePort, tunnelLocalHost, tunnelLocalPort);
		System.out.println("Connected");

	}
	
	class localUserInfo implements UserInfo {
		String passwd;
		public String getPassword() { return passwd; }
		public boolean promptYesNo(String str) { return true; }
		public String getPassphrase() { return null; }
		public boolean promptPassphrase(String message) { return true; }
		public boolean promptPassword(String message) { return true; }
		public void showMessage(String message) {}
	}   

}
