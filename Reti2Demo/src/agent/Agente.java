package agent;

import threads.Accendi;
import threads.DynamicPage;
import threads.Spegni;
import server.StartServer;
import wsclient.*;

import java.io.IOException;

import org.eclipse.paho.client.mqttv3.MqttException;
import mqtt.*;

public class Agente {

	static Subject root;
	static SubjectTable st;
	public static DynamicPage dp;
	
	public static void main(String[] args) {
		root = new Subject("root");
		st = new SubjectTable(root);
		
		new Accendi(st);
		new Spegni(st);
		
		ConnectionParams.setParams();
		
		try {
			StartServer.main(st);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		dp = new DynamicPage(st); 
		
		WSclient.main(st);
			
	}

}
