package agent;

import java.util.ArrayList;

public class MessageQueue {
	
	private static final int MAX = 10;
	private ArrayList<Message> queue; 
	private String threadId; 

	
	public MessageQueue(String threadId){
		this.queue= new ArrayList<>();
		this.threadId=threadId;	
	}
	
	public synchronized void send(Message m) { 
		if(queue.size() == MAX)
			queue.remove(0);
		queue.add(m);
		notify();
	} 
	
	public synchronized Message receive() {
		try {
			while (queue.isEmpty())
				wait();
		} catch (InterruptedException e) { e.printStackTrace(); }
		
		return queue.remove(0);
	}
	
	public String getId() {
		return threadId;
	}
	
	public ArrayList<Message> getMessages() {
		return queue;
	}

}
