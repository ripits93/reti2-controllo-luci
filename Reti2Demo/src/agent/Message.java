package agent;

public class Message {
	String method; 
	String uri;  
	String body;  
	
	public Message(String uri, String body) {
		this.uri = uri;
		this.body = body;
	}
	
	public Message(String mt, String uri, String body) {
		this.method = mt;
		this.uri = uri;
		this.body = body;
	}
	
	public String getUri() {
		return uri;
	}

	public String getBody(){
		return body;
	}
	
	public String getMethod(){
		return method;
	}
}
