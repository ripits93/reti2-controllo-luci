package agent;

import java.util.ArrayList;


public class Subject {
	private String name; 
	private ArrayList<MessageQueue> subscriber; 
	private ArrayList<Subject> son; 
	
	public Subject(String name){
		this.name=name;
		subscriber = new ArrayList<>();
		son = new ArrayList<>();
	}
	
	public ArrayList<MessageQueue> getMessQueue(){
		return subscriber;
	}
	
	public ArrayList<Subject> getSons(){
		return son;
	}
	
	public String getName(){
		return name;
	}
}
