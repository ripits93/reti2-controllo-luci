package agent;

public class News {
	String key, value;
	
	public News(String k, String v){
		key = k;
		value = v;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getValue(){
		return value;
	}
	
	public void setKey(String k){
		key = k;
	}
	
	public void setValue(String v){
		value = v;
	}
	
}