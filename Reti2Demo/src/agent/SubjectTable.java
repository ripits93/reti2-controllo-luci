package agent;

import java.util.ArrayList;


public class SubjectTable {
	
	private Subject root;
	private ArrayList<Subject> check; 
	
	
	public SubjectTable(Subject root){
		this.root = root; 
		check = new ArrayList<>();
	}
	
	public Subject getRoot(){
		return root;
	}
	

	public synchronized void subscribe(String url, MessageQueue mq){
		String[] separated = url.split("/");
		int i=0;
		subscribe_DFS(separated,i,mq,root);
	}
	

	private void subscribe_DFS(String[] nodes, int i, MessageQueue mq, Subject s){
		if(i==(nodes.length)-1){
			s.getMessQueue().add(mq);
			return;
		}
		
		for(Subject x:s.getSons()){
			if(x.getName().equals(nodes[i+1])){
				subscribe_DFS(nodes,i+1,mq,x);
				return;
			}
		}
		
		Subject y = new Subject(nodes[i+1]);
		s.getSons().add(y);
		subscribe_DFS(nodes,i+1,mq,y);
	}
	

	public synchronized void notify(Message m) {
		check.clear();
		String url = m.getUri();
		String[] separated = url.split("/");
		int i=0;
		notify_DFS(separated,root,i);
		if((check.size())==(separated.length)){
			for(Subject s:check){
				for(MessageQueue mq:s.getMessQueue()){
					mq.send(m);
				}
			}
		}
	}
	
	
	private void notify_DFS(String[] nodes, Subject s, int i) {
		check.add(s);
		if(i==(nodes.length)-1){
			return;
		}
		for(Subject son:s.getSons()){
			if(son.getName().equals(nodes[i+1])){
				notify_DFS(nodes,son,i+1);
				return;
			}
		}
	}
	
	
	public synchronized void byebye(String threadId){
		byebye_DFS(threadId,root);
	}
	
	
	private void byebye_DFS(String threadId,Subject s){
		for(MessageQueue m:s.getMessQueue()){
			if((m.getId()).equals(threadId)){
				s.getMessQueue().remove(m);
			}
		}
		for(Subject x:s.getSons()){
			byebye_DFS(threadId,x);
		}
		return;
	}
	
}

  
