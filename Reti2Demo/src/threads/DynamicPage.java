package threads;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agent.ConnectionParams;
import agent.Message;
import agent.MessageQueue;
import agent.News;
import agent.SubjectTable;

public class DynamicPage extends Thread {
	
	public String[] luci = new String[] 
			{ "LI_Luce1", "LI_Luce2", "LI_Luce3", "LI_Luce4", "LI_Luce5", "LI_Luce6", "LI_Luce7", "LI_Luce8" };
	
	public MessageQueue mq;
	public SubjectTable sm;
	
	News[] entry;
	
	public DynamicPage(SubjectTable sm) {
		this.entry = new News[8];
		
		this.sm = sm;
		this.mq = new MessageQueue("dynamicPage");
		this.sm.subscribe("root/threads/dynamicPage", mq);
		
		// inizializzazione dell'array entry
		initEntry();
		
		this.start();
	}
	
	private void initEntry(){
		
		for (int i = 0; i < luci.length; i++) {
			try {
				// ottengo lo stato della singola luce
				// la parsifico e poi la aggiungo all'array "entry"
				JSONObject jsonObj = new JSONObject(getStatoLuce(luci[i]));
			
				String stat = jsonObj.optJSONObject("response").optJSONObject("device").optString("status");

				entry[i] = new News(luci[i], stat);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private String getStatoLuce(String luce) throws Exception {
		String dati = new String("");
		URL url = new URL("http://"+ConnectionParams.params[7]+"/horus-ws/Devices/" + luce + ".json");
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
		    for (String line; (line = reader.readLine()) != null;) {
		        dati = dati.concat(line);
		    }
		}
		System.out.println("DYNAMIC PAGE: " + dati);
		return dati;
	}
	
	public synchronized void publish(String name, String value){
		for(int i=0; i<entry.length; i++){
			if(entry[i].getKey().equals(name)){
				entry[i].setValue(value);
			}
		}
	} 
    
	public synchronized String get(String name){
		String value = new String("");
		for(int i=0; i<entry.length; i++){
			if(entry[i].getKey().equals(name)){
				return entry[i].getValue();
			}
		}
		return null;
	}
	
	public String getJson() throws JSONException{
		JSONArray array = new JSONArray();
		
		for(News i : entry){
			array.put(new JSONObject().put(i.getKey(), i.getValue()));
		}
		
		JSONObject obj = new JSONObject();
		
		obj.put("luci", array);
		
		return obj.toString();
	}
	
	public void run() {
		
		try{
			
			while(true){
				
				System.out.println("DYNAMIC PAGE: in attesa di messaggi...");
				// wait for message from HttpServer 
				
				Message msg = this.mq.receive();
				
				if(msg.getMethod().equals("PUT")){
					String str = msg.getBody();
					JSONObject jsonObj = new JSONObject(str);					
					
					String name = jsonObj.optString("name");
					String value = jsonObj.optString("status");
				
					publish(name, value);
					
					for(News i : entry){
						System.out.println(i.getKey() + ", " + i.getValue());
					}
				} else if(msg.getMethod().equals("GET")){
					Message response = new Message("root/handlers/getLuci", getJson());
					this.sm.notify(response);	
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}

