package threads;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import agent.Agente;
import agent.ConnectionParams;
import agent.Message;
import agent.MessageQueue;
import agent.SubjectTable;

public class Spegni  extends Thread {

	String[] luci = new String[] { "LI_Luce1", "LI_Luce2", "LI_Luce3", "LI_Luce4",
			"LI_Luce5", "LI_Luce6","LI_Luce7", "LI_Luce8"};
	
	public String dati;
	public MessageQueue mq;
	public SubjectTable sm;
	
	public Spegni(SubjectTable sm){
		this.sm = sm;
		this.mq = new MessageQueue("spegni");
		this.sm.subscribe("root/threads/spegni", mq);
		
		this.start();
	}
	
	public void run() {
		
		try{
			
			while(true){
				String response = new String("");
				System.out.println("Thread spegni in attesa di ordini...");
				// 
				Message msg = this.mq.receive();
				
				if(msg.getBody().equals("ALL")){
					
					for(String i : luci){
						if(Agente.dp.get(i).equals("true")){
							Process p = Runtime.getRuntime().exec("curl "+ConnectionParams.params[7]+"/horus-ws/Devices/" + i + "/OFF.json -X PUT");
							BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
							String line;

							while ((line = in.readLine()) != null) {
								response += line;
							}
						
						}
					}					
					
					this.sm.notify(new Message("root/handlers/spegniHandler/all", response));
					
				}
				else {
					String luce = luci[Integer.parseInt(msg.getBody())];
					
					//if(Agente.dp.get(luce).equals("true")){
						Process p = Runtime.getRuntime().exec("curl "+ConnectionParams.params[7]+"/horus-ws/Devices/" + luce + "/OFF.json -X PUT");
						BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
						String line;
						
						while ((line = in.readLine()) != null) {
						    response += line;
						}
					//}
					// thread risponde all'handler "SpegniLuceHandler"
					this.sm.notify(new Message("root/handlers/spegniHandler/" + luce, response));
				}
				
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
}