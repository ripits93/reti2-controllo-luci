package threads;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import agent.Agente;
import agent.ConnectionParams;
import agent.Message;
import agent.MessageQueue;
import agent.SubjectTable;

public class Accendi extends Thread {

	String[] luci = new String[] { "LI_Luce1", "LI_Luce2", "LI_Luce3", "LI_Luce4",
			"LI_Luce5", "LI_Luce6", "LI_Luce7","LI_Luce8" };

	public String dati;
	public MessageQueue mq;
	public SubjectTable sm;

	public Accendi(SubjectTable sm) {
		this.sm = sm;
		this.mq = new MessageQueue("accendi");
		this.sm.subscribe("root/threads/accendi", mq);

		this.start();
	}

	public void run() {

		try {

			while (true) {
				String response = new String("");
				System.out.println("Thread accendi in attesa di ordini...");

				Message msg = this.mq.receive();

				if (msg.getBody().equals("ALL")) {

					for (String i : luci) {
						// accendo la luce
						if (Agente.dp.get(i).equals("false")) {

							Process p = Runtime.getRuntime()
									.exec("curl "+ConnectionParams.params[7]+"/horus-ws/Devices/" + i + "/ON.json -X PUT");

							BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
							String line;

							while ((line = in.readLine()) != null) {
								response += line;
							}

						}
					}

					// thread risponde all'handler "AllLuciOnHandler"

					this.sm.notify(new Message("root/handlers/accendiHandler/all", response));

				} else {
					String luce = luci[Integer.parseInt(msg.getBody())];

					if (Agente.dp.get(luce).equals("false")) {
						Process p = Runtime.getRuntime()
								.exec("curl "+ConnectionParams.params[7]+"/horus-ws/Devices/" + luce + "/ON.json -X PUT");
						BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
						String line;

						while ((line = in.readLine()) != null) {
							response += line;
						}
					}
	
					this.sm.notify(new Message("root/handlers/accendiHandler/" + luce, response));
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}