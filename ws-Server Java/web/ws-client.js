
var socket = new WebSocket("ws://localhost:8080/ws-server/java");
socket.onmessage = onMessage;

function onMessage(event) {
   var para = document.createElement("pre");
   var d = new Date();
   var n = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "  >>  ";
   var node = document.createTextNode(n + event.data);
   
   para.appendChild(node);

   var element = document.getElementById("content");
   var child = element.firstChild;
   
   if(element.childNodes.length > 19)
      element.removeChild(element.lastChild);
   
   element.insertBefore(para, child);
}