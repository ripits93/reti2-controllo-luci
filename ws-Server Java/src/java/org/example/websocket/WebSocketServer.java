package org.example.websocket;

import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/java")

public class WebSocketServer {
   
    
    @Inject
    private SessionHandler sessionHandler;
   
    @OnOpen
    public void onOpen(Session session) {
        sessionHandler.addSession(session);
    }
 
    @OnMessage
    public void onMessage(String message) {
        // Metodo eseguito alla ricezione di un messaggio
        // La stringa 'message' rappresenta il messaggio
 
        // Il valore di ritorno di questo metodo sara' automaticamente
        // inviato come risposta al client. Ad esempio:
        sessionHandler.sendToAllConnectedSessions(message);
          
    }
 
    @OnClose
    public void onClose(Session session) {
       // Metodo eseguito alla chiusura della connessione
       sessionHandler.removeSession(session);
    }
 
    @OnError
    public void onError(Throwable exception, Session session) {
        // Metodo eseguito in caso di errore
    }
}