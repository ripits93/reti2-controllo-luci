package org.example.websocket;

import java.io.IOException;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import javax.websocket.Session;

@ApplicationScoped
public class SessionHandler {
   
   private int deviceId = 0;
   private final Set<Session> sessions = new HashSet<>();

   public void addSession(Session session) {
      sessions.add(session);
//        for (Device device : devices) {
//            JsonObject addMessage = createAddMessage(device);
      sendToSession(session, "");
   }

   public void removeSession(Session session) {
      sessions.remove(session);
   }

    public void sendToAllConnectedSessions(String message) {
       sessions.forEach((session) -> {
          sendToSession(session, message);
      });
    }

    private void sendToSession(Session session, String message) {
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException ex) {
            sessions.remove(session);
            Logger.getLogger(SessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    
    
    
}