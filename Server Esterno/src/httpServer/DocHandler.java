package httpServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;



public class DocHandler implements HttpHandler {

	static String reqUri = new String("");
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		reqUri = he.getRequestURI().toString();
		System.out.println("DOC HANDLE reqUri: " + reqUri);
		System.out.println("DOC HANDLE Ip client: "+ he.getRemoteAddress().toString());
		
		try {
			if (reqUri.split("/")[2].equals("img"))
				sendImgFile(he);
			else
				sendTextFile(he);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public static void sendTextFile(HttpExchange hex) throws Exception{
		StringBuilder text = new StringBuilder("");
		try {
			File f = new File("." + reqUri);
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				text.append(line + "\n");
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		hex.sendResponseHeaders(200, text.length());

		OutputStream os = (OutputStream) hex.getResponseBody();
		os.write(text.toString().getBytes());
		os.close();
	}
	
	public static void sendImgFile(HttpExchange hex) throws Exception{
		Headers h = hex.getResponseHeaders();
		h.set("Content-Type", "image/png");
		
		hex.sendResponseHeaders(200, 0);
		OutputStream	os	=	(OutputStream) hex.getResponseBody();
		
		File f = new File("." + reqUri);
		FileInputStream fs = new FileInputStream(f);
	      final byte[] buffer = new byte[0x10000];
	      int count = 0;
	      while ((count = fs.read(buffer)) >= 0) {
	        os.write(buffer,0,count);
	      }
	      fs.close();
	      os.close();
		
	}

}
