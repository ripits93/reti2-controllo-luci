package httpServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

public class Start {

	static int port = 8081;

	public static void main(String[] args) {
		setVars();
		HttpServer server;
		try {
			server = HttpServer.create(new InetSocketAddress(port), 0);
			System.out.println("server	started	at	" + port);

			server.createContext("/", new RootHandler());
			server.createContext("/doc/", new DocHandler());

			server.setExecutor(null);

			server.start();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private synchronized static void setVars() {
		File file = new File("config.txt");
		if (file.exists() && !file.isDirectory()) {
			BufferedReader reader;
			try {
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
				String line = new String();
				while ((line = reader.readLine()) != null) {
					if (!line.startsWith("#") && !line.isEmpty()) {
						port = Integer.parseInt(line);
					}
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
