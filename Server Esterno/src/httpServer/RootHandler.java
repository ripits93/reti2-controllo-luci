package httpServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class RootHandler implements HttpHandler {

	static String reqUri = new String("");
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		reqUri = he.getRequestURI().toString();
		System.out.println("ROOT HANDLE reqUri: " + reqUri);
		System.out.println("ROOT HANDLE Ip client: "+ he.getRemoteAddress().toString());
		
		try {
			sendTextFile(he);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public static void sendTextFile(HttpExchange hex) throws Exception{
		StringBuilder text = new StringBuilder("");
		try {
			File f = new File("./doc/home.html");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				text.append(line + "\n");
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		hex.sendResponseHeaders(200, text.length());

		OutputStream os = (OutputStream) hex.getResponseBody();
		os.write(text.toString().getBytes());
		os.close();
	}

}
