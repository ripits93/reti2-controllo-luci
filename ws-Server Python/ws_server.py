import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import socket

 
class MyWebSocketServer(tornado.websocket.WebSocketHandler):
    global sessions
    sessions = []

    def sendBroadcast(self,message):
        for s in sessions:
            s.write_message(message)
        print 'Messaggio ricevuto: %s' % message
    
    def keepAlive(self):
        threading.Timer(5.0, printit).start()
        print "Hello, World!"
    
    def open(self):
        # metodo eseguito all'apertura della connessione
        sessions.append(self)
        print 'Nuova connessione', len(sessions),
       
    def on_message(self, message):
        # metodo eseguito alla ricezione di un messaggio
        # la stringa 'message' rappresenta il messaggio
        self.sendBroadcast(message)
  
    def on_close(self):
        # metodo eseguito alla chiusura della connessione
        sessions.remove(self)
        print 'Connessione chiusa', len(sessions)
  
    def check_origin(self, origin):
        return True
  
application = tornado.web.Application([
    (r'/ws-server/python', MyWebSocketServer),
])
  
  
if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(11024)
 
    tornado.ioloop.IOLoop.instance().start()
