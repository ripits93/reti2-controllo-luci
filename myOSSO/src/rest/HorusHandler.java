package rest;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class HorusHandler implements HttpHandler {

	static String reqUri = new String("");
	
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		System.out.println("HORUS HANDLE");
		System.out.println("reqUri: " + he.getRequestURI());
		System.out.println("Ip client: " + he.getRemoteAddress().toString());
		
		reqUri = he.getRequestURI().toString();
		System.out.println(reqUri);
		File myFoo = new File("./"+ reqUri);
		       
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(myFoo)));
		String line;
		
		String	response = new String("");
		while ((line = in.readLine()) != null) {
		    response += line;
		}
		in.close();
		System.out.println("RE: " + response);
		
		he.sendResponseHeaders(200,	response.length());
		
		OutputStream	os	=	(OutputStream) he.getResponseBody();
		os.write(response.getBytes());
		os.close();

	}

}
