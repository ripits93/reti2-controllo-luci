package rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import wsclient.WSclient;

public class AccendiLuceHandler implements HttpHandler {

	
	String idLuce;
	
	
	public AccendiLuceHandler(String luce){
		
		this.idLuce = luce;
	}
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		System.out.println("LuceOn" + idLuce + " HANDLE");
		System.out.println("Ip client: " + he.getRemoteAddress().toString());

		String response = "";
		
		if(he.getRequestMethod().equals("PUT")){
			System.out.println("PUT");
			File myFoo = new File("./horus-ws/Devices/"+idLuce+".json");
			FileOutputStream fooStream = new FileOutputStream(myFoo, false); // true to append
			                                                                 // false to overwrite.
			String update = "{\"response\":{\"message\":\"ok\",\"device\":{\"name\":\""+idLuce+"\",\"status\":\"true\"}}}"; 
			fooStream.write(update.getBytes());
			fooStream.close();
			String toSend = "{\"time\":\""+ new Date().getTime() +"\",\"message\":\"ok\",\"name\":\""+idLuce+"\",\"status\":\"true\"}";
			WSclient.socket.sendToSession(toSend);;
			response = update;
			System.out.println("AccendiLuceHandler: " + response); 
			
		} else {
			response = "<h1>Risorsa non trovata!</h1> \n";
		}
		
		he.sendResponseHeaders(200,	response.length());
		
		OutputStream	os	=	(OutputStream) he.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}

}
