package rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import rest.AccendiLuceHandler;
import rest.HorusHandler;
import rest.SpegniLuceHandler;

public class SimpleHttpServer {
	private int port;
	private HttpServer server;
	static String[] luci = new String[] { "LI_Luce1", "LI_Luce2", "LI_Luce3", "LI_Luce4", "LI_Luce5", "LI_Luce6","LI_Luce7", "LI_Luce8"};
	

	public void Start(int port) {
		try {
			this.port = port;
			server = HttpServer.create(new InetSocketAddress(port), 0);
			System.out.println("server started at " + port);
			
//			server.createContext("/", new Handlers.RootHandler());
			
			server.createContext("/horus-ws/Devices/", new HorusHandler());
			
			
			for(int i=0; i<8; i++){
				server.createContext("/horus-ws/Devices/" + luci[i] + "/ON.json", new AccendiLuceHandler(luci[i]));
				server.createContext("/horus-ws/Devices/" + luci[i] + "/OFF.json", new SpegniLuceHandler(luci[i]));
			}
		
			
			server.setExecutor(null);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void Stop() {
		server.stop(0);
		System.out.println("server stopped");
	}
}
