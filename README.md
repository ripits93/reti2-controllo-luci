
[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

# Progetto d'esame del corso Reti 2 (2017)
Questo è un progetto d'esame del corso di reti2  del dipartimento di Scienze ed Innovazione Tecnologica del Università degli Studi del Piemonte Orientale Amedeo Avogadro.

Trattasi di applicazione web che offre la possibilità di monitorare ed alterare lo stato di un insieme di luci, installate su una BeagleBone collocata in una rete privata. l'applicazione offre la possibilità di interagire con le luci anche da rete esterna (tramite tunnel ssh e broker mosquitto) a patto di disporre di una macchina che sia visibile anche dalla rete esterna, oltre che da quella privata.

Oltre al progetto vero e proprio sono state sviluppate altre applicazioni che ne  consentono il corretto funzionamento anche in ambiente domestico.

# Contenuto
Il repo contiene 5 applicazioni:  
  -  Reti2Demo: progetto d'esame vero e proprio;  
  -  myOSSO: semplice web server java al quale il progetto invia le chiamate rest;  
  -  ws-server py: semplice implementazione python 2.7 di un server websocket (solo unix);  
  -  ws-server Java: progetto netbeans che implementa un websocket server scritto in java (tutte le piattaforme);  
  -  Server Esterno: un server java che permette di accedere al client mosquitto javascript  

# Requisiti
Occorre installare:  
[Curl](https://curl.haxx.se) - necessario per le chiamate http al server myOsso  
[Eclipse](https://www.eclipse.org) -  per i server Reti2Demo, myOsso e Server Esterno  
[Netbeans](https://netbeans.org) - per il ws server java  
[Tornando](http://www.tornadoweb.org/en/stable/) - ws server python (solo unix)  
[Glassfish](https://docs.oracle.com/cd/E19798-01/821-1770/gioew/index.html) - necessario per avviare il ws server java su Netbeans

# Istruzioni per l'uso
È possibile interagire con l'applicazione sia tramite chiamate curl da linea di comando, sia tramite browser.

Tutti i parametri di connessione (indirizzi e porte) del progetto Reti2Demo sono contenuti nel file di testo config.txt: editare tali parametri in base alle proprie esigenze, rispettando i formati mostrati per ogni sezione.

Occorre editare anche valore wsuri presente in /doc/luci.html per la connessione al websocket server (nel esempio è mostrato l'url del ws server java):
```sh
34 var wsuri = "ws://"+host+":8080/ws-server/java";
```
Gli url associati ai ws server python e java sono, rispettivamente:
>ws://localhost:11021/ws-server/python  
ws://localhost:8080/ws-server/java

È possibile modificare la porta di accesso al ws server pyhton modificando la seguente riga del file /ws-server python/ws_server.py:
```sh
38 http_server.listen("ws server port")
```
##### Avvio
Avviare nel seguente ordine:  
1  un server webSocket  
2  server myOsso  
3  progetto Reti2Demo

### MOSQUITTO
Se si vuole utilizzare la connessione mosquitto occorre un broker mosquitto con supporto alle webSocket abilitato al quale connettersi. Occorre impostare anche ip e porta del broker nel file /Server Esterno/doc/luci.html:
```sh
31 client = new Paho.MQTT.Client("server ip", Number("server port"), ...);
```
Questo consente al client javascript di connettersi al server mosquitto. Per permettere anche al progetto Reti2Demo di connettersi occorre specificare l'url del server mosquitto nel file /Reti2Demo/config.txt

##### Abilitare supporto websocket per mosquitto (LINUX):
Abilitare il supporto alle connessioni websocket è necessario perchè sono sfruttate dalle librerie eclipse paho del client mqtt javascript.   
Aprire come amministratore il file /etc/mosquitto/mosquitto.conf:
```sh
$ sudo nano /etc/mosquitto/mosquitto.conf
```
ed aggiungere in fondo le seguenti 3 righe:

>port 1883  
listener 1884  
protocol websocket

Successivamente occorre riavviare il servizio mosquitto:
```sh
$ sudo service mosquitto restart
```
Questo permette al server di ricevere connessioni tcp alla porta 1883 e connessioni websocket alla porta 1884

### TUNNEL SSH:
Nel file config.txt occorre specificare (nella sezione tunnel ssh) rispettivamente:  
-  Porta remota,  
-  Ip remoto,  
-  Utente remoto,  
-  Password remota,  
-  Porta websocket in uso (infatti occorre aprire due tunnel: uno per connettersi al server Reti2Demo ed uno per connettersi al server webSocket)

##### Inoltro traffico in arrivo sul host remoto (LINUX)

Quando si chiede all'sshd (il demon in che gestisce ssh sull'host remoto) di fare l'inoltro del traffico in arrivo sulla sua porta specificata bisogna dare una autorizzazione a livello di configurazione di sistema.  
In particolare bisogna editare il file di configurazione:  
```sh
$ sudo nano /etc/ssh/sshd_config
```
ed aggiungere in fondo al file la seguente riga:
>GatewayPorts yes

A quel punto riavviare il servizio:  
```sh
$ sudo service ssh restart
```
In questo modo, una volta creato il tunnel, il traffico in arrivo sul host remoto viene inoltrato al server Reti2Demo